# HTML and text utilities
Assists in generating pretty HTML and indented text

## What is this?

It's a Python 3 library that assists in generating pretty HTML and indented text.

## Why would I want this?

If you're programmatically generating HTML or (indented) text, give this
library a look.

## Status

This software is alpha, and needs testing and feedback.

## How do I use this?

### Method A:

* Drop the file "indenttext.py" into your project.
* In your project file, do `import indenttext`.
* Read the code and examples.

### Method B:

* Clone the git repository into a directory one level above your project.

e.g.
```
some_dir
  ├─ my_project
  └─ indenttext
```

* Make sure `../indenttext` is in your `PYTHONPATH`, e.g. by running your project from a script that adds it to `PYTHONPATH`.
* In your project file, do `import indenttext`.
* Read the code and examples.

## Author

[Coffee](https://gitlab.com/Matrixcoffee)

## Contact
* Website: [http://open.source.coffee](http://open.source.coffee)
* Mastodon: [@Coffee@toot.cafe](https://toot.cafe/@Coffee)

## License

Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

The full text of this license can be found in the file called
[LICENSE](https://gitlab.com/Matrixcoffee/odotrack-app-basic/blob/master/LICENSE).
