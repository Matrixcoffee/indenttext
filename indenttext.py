import collections
import html

class IndentText:
	def __init__(self, indent=None):
		self.indent = indent
		if self.indent is None: self.indent = "  "
		self.items = collections.deque()

	def __iter__(self):
		for line in self.__iter2__():
			if line.strip() == "": yield ""
			else: yield line

	def __iter2__(self):
		for item in self.items:
			if isinstance(item, str):
				yield self.indent + item
			else:
				for item2 in item:
					yield self.indent + item2

	def append(self, *items):
		for item in items:
			self.items.append(item)
		return self


class HTMLTag(IndentText):
	def __init__(self, tagname, attrs=None, flow=False, container=True, indent=None, **attrs2):
		if attrs is None: attrs = attrs2
		self.tagname = tagname
		self.attrs = attrs
		self.flow = flow
		self.container = container
		super().__init__(indent)
		if not self.container: self.items = ()

	def __iter__(self):
		opentag = "<" + self.tagname
		if self.attrs:
			for k, v in self.attrs.items():
				opentag += " {}=\"{}\"".format(html.escape(k), html.escape(v))
		opentag += ">"

		if self.container:
			closetag = "</{}>".format(self.tagname)

		if self.flow and len(self.items) <= 1:
			yield opentag + " ".join(x.strip() for x in super().__iter__()) + closetag
		else:
			yield opentag

			for item in super().__iter__():
				yield item

			if self.container:
				yield closetag


if __name__ == '__main__':
	test = IndentText(indent="")
	test2 = IndentText()
	test3 = IndentText()
	test4 = IndentText()

	test.append("<table>")
	test.append(test2)
	test2.append("<tr>")
	test2.append(test3)
	test3.append("<td>")
	test3.append(test4)
	test4.append("hello")
	test3.append("</td>")
	test2.append("</tr>")
	test.append("</table")

	for line in test:
		print(line)

	print("------------")

	t = HTMLTag("table")
	r = HTMLTag("tr")
	t.append(r)
	c = HTMLTag("td")
	r.append(c)
	a = HTMLTag("a", href="hello.txt", flow=True)
	c.append(a)
	a.append("hello")
	c.append("blank line follows")
	c.append("")

	for line in t:
		print(line)
